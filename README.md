#Notice
Before running main.py file you need to install those following packages:
1. speech_recognition
2. urllib
3. pyttsx3

You can test and choose appropriate voices in testvoice.py file
Replace the chosen voice_id in main.py

To use sending email command, turn off gmail 2-step verification and turn on less security

#Supported commands:
1. Call me xyz: Register master name
2. Open xyz.com : replace xyz with any website name
3. Hello
4. Help me: list of supported commands
5. Joke: tell a random joke
6. News for today: reads top news of today
7. Current weather in {cityname} : Tells you the current condition and temperture 
8. time : Current system time
9. Send email/email : Follow up questions such as recipient name, content will be asked in order. 
10. Tell me about xyz : tells you about xyz