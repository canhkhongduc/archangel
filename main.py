import speech_recognition as sr
import os
import sys
import re
import webbrowser
import smtplib
import requests
from pyowm import OWM
import urllib
import json
from bs4 import BeautifulSoup as soup
from urllib.request import urlopen
import wikipedia
from time import strftime
import pyttsx3
engine = pyttsx3.init()
def assistantResponse(audio):
    "speaks audio passed as argument"
    print(audio)
    engine.setProperty('voice', 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_EN-US_ZIRA_11.0')
    engine.say(audio)
    engine.runAndWait()
def myCommand():
    "listens for commands"
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print('Say something...')
        r.pause_threshold = 1
        r.adjust_for_ambient_noise(source, duration=1)
        audio = r.listen(source)
    try:
        command = r.recognize_google(audio).lower()
        print('You said: ' + command + '\n')
    #loop back to continue to listen for commands if unrecognizable speech is received
    except sr.UnknownValueError:
        print('....')
        command = myCommand()
    return command
name = ""
def assistant(command):
    
    "if statements for executing commands"
#register name
    if 'call me' in command:
        reg_ex = re.search('call me (.*)', command)
        global name
        name = reg_ex.group(1)
        assistantResponse("I understood " + reg_ex.group(1))
#open website
    elif 'open' in command:
        reg_ex = re.search('open (.+)', command)
        if reg_ex:
            domain = reg_ex.group(1)
            print(domain)
            url = 'https://www.' + domain
            webbrowser.open(url)
            assistantResponse('The website you have requested has been opened for you ' + name)
        else:
            pass
#greetings
    elif 'hello' in command:
        day_time = int(strftime('%H'))
        if day_time < 12:
            assistantResponse('Hello master. Good morning')
        elif 12 <= day_time < 18:
            assistantResponse('Hello master. Good afternoon')
        else:
            assistantResponse('Hello master. Good evening')
    elif 'help me' in command:
        assistantResponse("""
        You can use these commands and I'll help you out:
        1. Call me xyz: Register master name
        2. Open xyz.com : replace xyz with any website name
        3. Hello
        4. Help me: list of supported commands
        5. Joke: tell a random joke
        6. news for today: reads top news of today
        7. Current weather in {cityname} : Tells you the current condition and temperture 
        8. time : Current system time
        9. Send email/email : Follow up questions such as recipient name, content will be asked in order.
        3. 
        5. change wallpaper : Change desktop wallpaper
        6. news for today : reads top news of today
        7. 
        8. top stories from google news (RSS feeds)
        9. tell me about xyz : tells you about xyz
        """)
#joke
    elif 'joke' in command:
        res = requests.get(
                'https://icanhazdadjoke.com/',
                headers={"Accept":"application/json"})
        if res.status_code == requests.codes.ok:
            assistantResponse(str(res.json()['joke']))
        else:
            assistantResponse('oops!I ran out of jokes')
#top stories from google news
    elif 'news for today' in command:
        try:
            news_url="https://news.google.com/news/rss"
            Client=urlopen(news_url)
            xml_page=Client.read()
            Client.close()
            soup_page=soup(xml_page,"xml")
            news_list=soup_page.findAll("item")
            for news in news_list[:15]:
                assistantResponse(news.title.text.encode('utf-8'))
        except Exception as e:
                print(e)
#current weather
    elif 'current weather' in command:
        reg_ex = re.search('current weather in (.*)', command)
        if reg_ex:
            city = reg_ex.group(1)
            owm = OWM(API_key='ab0d5e80e8dafb2cb81fa9e82431c1fa')
            obs = owm.weather_at_place(city)
            w = obs.get_weather()
            k = w.get_status()
            x = w.get_temperature(unit='celsius')
            assistantResponse('Current weather in %s is %s. The maximum temperature is %0.2f and the minimum temperature is %0.2f degree celcius' % (city, k, x['temp_max'], x['temp_min']))
#time
    elif 'time' in command:
        import datetime
        now = datetime.datetime.now()
        assistantResponse('Current time is %d hours %d minutes' % (now.hour, now.minute))
    elif 'email' in command:
        assistantResponse('Who is the recipient?')
        recipient = myCommand()
        if 'girlfriend' in recipient:
            assistantResponse('What should I say to her?')
            content = myCommand()
            mail = smtplib.SMTP('smtp.gmail.com', 587)
            mail.ehlo()
            mail.starttls()
            mail.login('your-email', 'your-password')
            mail.sendmail('sender-email', 'receiver-email', content)
            mail.close()
            assistantResponse('Email has been sent successfuly. You can check your inbox.')
            webbrowser.open("https://mail.google.com/mail/u/0/#sent")
        else:
            assistantResponse('I don\'t know what you mean!')
#play youtube song
    elif 'play me a song' in command:
        assistantResponse('What song shall I play? ' + name)
        mysong = myCommand()
        if mysong:
            flag = 0
            url = "https://www.youtube.com/results?search_query=" + mysong.replace(' ', '+')
            response = urlopen(url)
            html = response.read()
            soup1 = soup(html,"lxml")
            for vid in soup1.findAll(attrs={'class':'yt-uix-tile-link'}):
                if ('https://www.youtube.com' + vid['href']).startswith("https://www.youtube.com/watch?v="):
                    flag = 1
                    final_url = 'https://www.youtube.com' + vid['href']
                    webbrowser.open(final_url)
                    break
            if flag == 0:
                assistantResponse('I have not found anything in Youtube ')
#askme anything
    elif 'tell me about' in command:
        reg_ex = re.search('tell me about (.*)', command)
        try:
            if reg_ex:
                topic = reg_ex.group(1)
                ny = wikipedia.page(topic)
                assistantResponse(ny.content[:500].encode('utf-8'))
        except Exception as e:
                print(e)
                assistantResponse(e)
assistantResponse('Hello master, I am X Æ A-12 and I am your virtual personal assistant. How may I call you?')
#loop to continue executing multiple commands
while True:
    assistant(myCommand())